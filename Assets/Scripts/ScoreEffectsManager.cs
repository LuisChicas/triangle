﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScoreEffectsManager : SingletonM<ScoreEffectsManager>
{
    [SerializeField]
    private Canvas _canvas;
    [SerializeField]
    private GameObject _additionEffectPrefab;

    private List<GameObject> _additionEffects = new List<GameObject>();

    public void ShowAdditionEffect(Vector3 position, int score)
    {
        GameObject effect = GetAdditionEffect();
        effect.GetComponent<RectTransform>().anchoredPosition = PositionHelper.WorldToCanvasPosition(position, _canvas);
        effect.GetComponent<ScoreAdditionEffect>().Show(score);
    }

    private GameObject GetAdditionEffect()
    {
        GameObject effect = _additionEffects.Where(e => !e.activeSelf).FirstOrDefault();

        if (effect != default(GameObject))
        {
            effect.gameObject.SetActive(true);
        }
        else
        {
            effect = Instantiate(_additionEffectPrefab, transform);
            _additionEffects.Add(effect);
        }

        return effect;
    }

    public void Clear()
    {
        if (_additionEffects.Any(e => e.activeSelf))
        {
            _additionEffects.Where(e => e.activeSelf).ToList().ForEach(e => e.SetActive(false));
        }
    }
}
