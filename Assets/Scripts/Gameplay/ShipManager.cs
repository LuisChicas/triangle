﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : SingletonM<ShipManager>
{
    private const float FingerOffset = 1;

    [SerializeField]
    private GameObject _shootPrefab;

    private float _shotsPerSecond;

    private float _maxMovementDelta = 0.4f;
    private Vector3 _inputScreenPosition;
    private Vector3 _inputWorldPosition;

    public void StartShooting()
    {
        _shotsPerSecond = 2;
        StartCoroutine(ShootRoutine());
    }

    private IEnumerator ShootRoutine()
    {
        while(true)
        {
            Instantiate(_shootPrefab, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(1 / _shotsPerSecond);
            yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
        }
    }

    public void AddShotsPerSecond(float value)
    {
        _shotsPerSecond += value;
    }

    private void Update()
    {
        if (GameplayManager.Instance.State != GameplayState.Playing) return;

#if UNITY_EDITOR
        CheckMouseInput();
#else
		CheckTouchInput ();
#endif
    }

    private void CheckTouchInput()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            bool isValidPhase = touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary;

            if (isValidPhase)
            {
                _inputScreenPosition = new Vector3(touch.position.x, touch.position.y, Camera.main.nearClipPlane);
                _inputWorldPosition = Camera.main.ScreenToWorldPoint(_inputScreenPosition);
                _inputWorldPosition.y += FingerOffset;
                transform.Translate(Vector3.ClampMagnitude(_inputWorldPosition - transform.position, _maxMovementDelta));
            }
        }
    }

    private void CheckMouseInput()
    {
        if (Input.GetMouseButton(0))
        {
            _inputScreenPosition = Input.mousePosition;
            _inputScreenPosition.z = Camera.main.nearClipPlane;
            _inputWorldPosition = Camera.main.ScreenToWorldPoint(_inputScreenPosition);
            transform.Translate(Vector3.ClampMagnitude(_inputWorldPosition - transform.position, _maxMovementDelta));
        }
    }
}
