﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Triangle.PatternEffects
{
    public class ItemRandomOffset
    {
        public static void Apply(Pattern pattern, Dictionary<string, object> values)
        {
            string itemNameString = values["itemName"].ToString();
            ItemName itemName = (ItemName)Enum.Parse(typeof(ItemName), itemNameString);

            float xMin = 0;
            float xMax = 0;
            float yMin = 0;
            float yMax = 0;

            if (values.ContainsKey("xMin"))
            {
                xMin = float.Parse(values["xMin"].ToString());
                xMax = float.Parse(values["xMax"].ToString());
            }

            if (values.ContainsKey("yMin"))
            {
                yMin = float.Parse(values["yMin"].ToString());
                yMax = float.Parse(values["yMax"].ToString());
            }

            PatternItem[] items = pattern.Items.Where(i => i.ItemName == itemName).ToArray();

            for (int i = 0; i < items.Length; i++)
            {
                items[i].X += UnityEngine.Random.Range(xMin, xMax);
                items[i].Y += UnityEngine.Random.Range(yMin, yMax);
            }
        }
    }
}
