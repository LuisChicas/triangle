﻿using System;
using System.Collections.Generic;
using Triangle.PatternEffects;

public class PatternEffect
{
    public string Type { get; set; }
    public Dictionary<string, object> Values { get; set; }

    public new PatternEffectType GetType() =>
        (PatternEffectType)Enum.Parse(typeof(PatternEffectType), Type);

    public void ApplyTo(Pattern pattern)
    {
        switch (GetType())
        {
            case PatternEffectType.ItemRandomOffset:
                ItemRandomOffset.Apply(pattern, Values);
                break;

            case PatternEffectType.PatternRandomOffset:
                PatternRandomOffset.Apply(pattern, Values);
                break;

            case PatternEffectType.PatternVersions:
                PatternVersions.Apply(pattern, Values);
                break;

            case PatternEffectType.PatternGeneration:
                PatternGeneration.Apply(pattern, Values);
                break;
        }
    }
}
