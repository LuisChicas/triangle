﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternRandomOffset
{
    public static void Apply(Pattern pattern, Dictionary<string, object> values)
    {
        float xOffset = 0;
        float yOffset = 0;

        if (values.ContainsKey("xMin"))
        {
            float xMin = float.Parse(values["xMin"].ToString());
            float xMax = float.Parse(values["xMax"].ToString());

            xOffset = Random.Range(xMin, xMax);
        }

        if (values.ContainsKey("yMin"))
        {
            float yMin = float.Parse(values["yMin"].ToString());
            float yMax = float.Parse(values["yMax"].ToString());

            yOffset = Random.Range(yMin, yMax);
        }

        for (int i = 0; i < pattern.Items.Length; i++)
        {
            pattern.Items[i].X += xOffset;
            pattern.Items[i].Y += yOffset;
        }
    }
}
