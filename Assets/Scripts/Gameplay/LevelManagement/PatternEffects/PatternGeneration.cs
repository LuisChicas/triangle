﻿using System.Collections.Generic;
using UnityEngine;

public class PatternGeneration 
{
    private static readonly Dictionary<ItemName, float> ItemsFrecuencies = new Dictionary<ItemName, float>()
    {
        { ItemName.Asteroid, 0.7f },
        { ItemName.Comet, 0.05f },
        { ItemName.DoubleAsteroid, 0.05f },
        { ItemName.TripleAsteroid, 0.05f },
        { ItemName.BouncingAsteroid, 0.05f },
        { ItemName.Spawner, 0.05f },
        { ItemName.LongAsteroid, 0.03f },
        { ItemName.Bomb, 0.03f }
    };

    public static void Apply(Pattern pattern, Dictionary<string, object> values)
    {
        List<PatternItem> items = new List<PatternItem>();
        int verticalPosition = 0;

        const int BaseWavesPerPowerUp = 7;

        PatternItem item;
        int wavesPerIndex;
        int itemsPerWave;
        int totalWaves = 0;

        int nextPowerUpWave = BaseWavesPerPowerUp + Random.Range(-1, 1);
        int powerUpIndex = 0;

        for (int i = 7; i < 8; i++)
        {
            wavesPerIndex = (int)System.Math.Round(i * 0.5f, System.MidpointRounding.AwayFromZero);

            for (int j = 0; j < wavesPerIndex; j++)
            {
                itemsPerWave = Random.Range((int)(i * 0.5f), i);
                powerUpIndex = Random.Range(0, itemsPerWave);

                for (int k = 0; k < itemsPerWave; k++)
                {
                    item = new PatternItem();

                    if (nextPowerUpWave == totalWaves && powerUpIndex == k)
                    {
                        item.Name = ItemName.FrecuenceBooster.ToString();
                        nextPowerUpWave += BaseWavesPerPowerUp + Random.Range(-1, 1);
                    }
                    else
                    {
                        item.Name = GetItem().ToString();
                    }

                    item.X = Random.Range(5, 95);

                    verticalPosition += Random.Range(7, 25);
                    item.Y = verticalPosition;

                    items.Add(item);
                }

                totalWaves++;
                verticalPosition += 50;
            }
        }

        pattern.Items = items.ToArray();
    }

    private static ItemName GetItem()
    {
        float totalValue = 0;
        foreach (var frecuency in ItemsFrecuencies)
        {
            totalValue += frecuency.Value;
        }

        ItemName name = ItemName.Asteroid;
        float randomValue = Random.Range(0f, totalValue);

        foreach (var frecuency in ItemsFrecuencies)
        {
            if (randomValue < frecuency.Value)
            {
                name = frecuency.Key;
                break;
            }

            randomValue -= frecuency.Value;
        }

        return name;
    }
}
