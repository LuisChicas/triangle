﻿
public enum PatternEffectType
{
    ItemRandomOffset,
    PatternRandomOffset,
    PatternVersions,
    PatternGeneration
}