﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PatternVersions
{
    public static void Apply(Pattern pattern, Dictionary<string, object> values)
    {
        int versions = int.Parse(values["number"].ToString());
        int randomVersion = Random.Range(0, versions);

        pattern.Items.Where(i => i.ValuesX != null || i.ValuesY != null).ToList().ForEach(i =>
        {
            if (i.ValuesX != null && i.ValuesX.Any())
            {
                i.X = i.ValuesX[randomVersion];
            }

            if (i.ValuesY != null && i.ValuesY.Any())
            {
                i.Y = i.ValuesY[randomVersion];
            }
        });
    }
}
