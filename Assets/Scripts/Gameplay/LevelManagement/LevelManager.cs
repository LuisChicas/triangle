﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using YamlDotNet.Samples.Helpers;

public class LevelManager : SingletonM<LevelManager>
{
    private const float OffscreenYPosition = 6.5f;
    private const float Acceleration = 0.02f;
    private const float InitialSpeed = 2;
    private const int BaseWavesPerPowerUp = 7;

    [SerializeField]
    private Transform _itemsContainer;

    [HideInInspector]
    public float Speed;

    private float _offscreenXPosition;
    private List<Transform> _onscreenItems = new List<Transform>();
    private List<Transform> _offscreenItems = new List<Transform>();
    private List<GameObject> _itemsPrefabs = new List<GameObject>();
    private float _distance = 0;

    private void Start()
    {
        _itemsPrefabs = Resources.LoadAll<GameObject>("Items").ToList();
        _offscreenXPosition = -Camera.main.ScreenToWorldPoint(Vector3.right * Screen.width * 0.1f).x;
    }

    public void StartLevel()
    {
        _distance = 0;
        Speed = InitialSpeed;

        StartCoroutine(Move());
        StartCoroutine(GenerateItems());
    }

    private IEnumerator GenerateItems()
    {
        TextAsset[] patternsAssets = Resources.LoadAll<TextAsset>("Patterns");

        var yamlDeserializer = new DeserializerBuilder()
                .WithNamingConvention(new CamelCaseNamingConvention())
                .Build();

        Vector3 screenUpperLeftPoint = Camera.main.ScreenToWorldPoint(new Vector3(
                0,
                Screen.height,
                Camera.main.nearClipPlane));

        float screenLeftLimit = screenUpperLeftPoint.x;
        float screenTopLimit = screenUpperLeftPoint.y;
        float screenWidth = Mathf.Abs(screenLeftLimit) * 2;
        float positionUnit = screenWidth / 100;

        int designedPatternsInRow = 0;

        while (true)
        {
            Pattern pattern;

            if (designedPatternsInRow < 2)
            {
                int randomPattern = Random.Range(0, patternsAssets.Length);
                pattern = yamlDeserializer.Deserialize<Pattern>(patternsAssets[randomPattern].text);

                designedPatternsInRow++;
            }
            else
            {
                pattern = new GeneratedPatternPrototype();

                designedPatternsInRow = 0;
            }

            pattern.Init();

            PatternItem patternItem;
            Transform item;
            float initialDistance = _distance;

            for (int i = 0; i < pattern.Items.Length; i++)
            {
                patternItem = pattern.Items[i];

                yield return new WaitUntil(() => (_distance - initialDistance) >= patternItem.Y * positionUnit);

                item = GetItem(patternItem.ItemName);
                item.transform.position = new Vector3(
                    screenLeftLimit + patternItem.X * positionUnit,
                    item.transform.position.y);

                item.GetComponent<Item>().Init(patternItem.Parameters);
                _onscreenItems.Add(item);
            }

            yield return new WaitForSeconds(1.5f);
            yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
        }
    }
    
    private IEnumerator Move()
    {
        while (true)
        {
            for (int i = 0; i < _onscreenItems.Count; i++)
            {
                _onscreenItems[i].Translate(Vector3.down * Time.deltaTime * Speed, Space.World);
            }

            _distance += Speed * Time.deltaTime;
            Speed += Acceleration * Time.deltaTime;

            yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
        }
    }
    
    private Transform GetItem(ItemName name)
    {
        Transform item = _offscreenItems.Where(i => i.GetComponent<Item>().Name == name).FirstOrDefault();
        if (item != default(Transform))
        {
            item.transform.position = new Vector3(0, OffscreenYPosition);
            item.gameObject.SetActive(true);
            _offscreenItems.Remove(item);
        }
        else
        {
            GameObject[] prefabs = _itemsPrefabs.Where(i => i.GetComponent<Item>().Name == name).ToArray();
            GameObject prefab = prefabs[Random.Range(0, prefabs.Length)];
            var itemInstance = Instantiate(prefab, new Vector3(0, OffscreenYPosition), Quaternion.identity, _itemsContainer) as GameObject;
            item = itemInstance.transform;
        }
        return item;
    }

    public Transform GenerateItem(ItemName name)
    {
        Transform item = GetItem(name);

        float horizontalPosition = Random.Range(-_offscreenXPosition, _offscreenXPosition);
        item.transform.position = new Vector3(horizontalPosition, item.transform.position.y);

        item.GetComponent<Item>().Init();
        _onscreenItems.Add(item);

        return item;
    }

    public void RemoveItem(Transform item)
    {
        item.gameObject.SetActive(false);
        _onscreenItems.Remove(item);
        _offscreenItems.Add(item);
    }

    public void RemoveItemPermanently(Transform item)
    {
        _onscreenItems.Remove(item);
    }

    public void StopLevel()
    {
        StopAllCoroutines();
        var items = new List<Transform>(_onscreenItems);
        for (int i = 0; i < items.Count; i++)
        {
            items[i].GetComponent<Item>().IsOnScreen = false;
            RemoveItem(items[i]);
        }
    }
}
