﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pattern
{
    public PatternItem[] Items { get; set; }

    public PatternEffect[] Effects { get; set; }

    public void Init()
    {
        SetRandomPositions();

        ApplyEffects();

        OrderItemsByVerticalPosition();
    }

    private void SetRandomPositions()
    {
        if (Items != null && Items.Any())
        {
            foreach (var item in Items)
            {
                item.SetRandomPositions();
            }
        }
    }

    private void ApplyEffects()
    {
        if (Effects != null && Effects.Any())
        {
            foreach (var effect in Effects)
            {
                effect.ApplyTo(this);
            }
        }
    }
    
    private void OrderItemsByVerticalPosition()
    {
        Items = Items.OrderBy(i => i.Y).ToArray();
    }
}
