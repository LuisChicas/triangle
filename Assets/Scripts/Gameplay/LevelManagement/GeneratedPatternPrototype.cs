﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratedPatternPrototype : Pattern
{
    public GeneratedPatternPrototype()
    {
        var generationEffect = new PatternEffect()
        {
            Type = PatternEffectType.PatternGeneration.ToString()
        };

        Effects = new PatternEffect[] { generationEffect };
    }
}
