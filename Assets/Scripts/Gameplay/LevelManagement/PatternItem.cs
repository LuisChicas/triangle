﻿using System;
using System.Collections.Generic;

public class PatternItem
{
    public string Name { get; set; }

    public float X { get; set; }
    public float Y { get; set; }

    public string RandomX { get; set; }
    public string RandomY { get; set; }

    public int[] ValuesX { get; set; }
    public int[] ValuesY { get; set; }

    public Dictionary<string, object> Parameters { get; set; }

    public ItemName ItemName => (ItemName)Enum.Parse(typeof(ItemName), Name);

    public void SetRandomPositions()
    {
        if (!string.IsNullOrEmpty(RandomX))
        {
            string[] rangeValues = RandomX.Split('-');
            float minValue = float.Parse(rangeValues[0]);
            float maxValue = float.Parse(rangeValues[1]);

            X = UnityEngine.Random.Range(minValue, maxValue);
        }

        if (!string.IsNullOrEmpty(RandomY))
        {
            string[] rangeValues = RandomY.Split('-');
            float minValue = float.Parse(rangeValues[0]);
            float maxValue = float.Parse(rangeValues[1]);

            Y = UnityEngine.Random.Range(minValue, maxValue);
        }
    }
}

