﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameplayState
{
    Idle,
    Playing,
    Paused
}

public class GameplayManager : SingletonM<GameplayManager>
{
    private const string GameOverScorePrefix = "<size=\"45\">score: </size>";
    private readonly Vector3 ShipInitialPosition = Vector3.down * 2;

    public int Score;
    public GameplayState State;

    [SerializeField]
    private GameObject _homeContainer;
    [SerializeField]
    private Text _homeHighscoreLabel;

    [SerializeField]
    private GameObject _hudContainer;
    [SerializeField]
    private Text _scoreLabel;
    [SerializeField]
    private GameObject _pauseButton;
    [SerializeField]
    private GameObject _resumeButton;

    [SerializeField]
    private GameObject _gameOverUIContainer;
    [SerializeField]
    private Text _gameOverScoreLabel;
    [SerializeField]
    private Text _gameOverHighscoreLabel;
    [SerializeField]
    private RectTransform _gameOverNewHighscoreLabel;

    [SerializeField]
    private AudioClip _menuMusic;
    [SerializeField]
    private AudioClip _gameplayMusic;

    private void Start()
    {
#if UNITY_EDITOR
        AudioListener.volume = 0;
#endif

        State = GameplayState.Idle;
        _homeHighscoreLabel.text = PlayerPrefs.GetInt("Highscore").ToString();
        ShipManager.Instance.gameObject.SetActive(false);
    }

    public void AddScore(int score)
    {
        Score += score;
        _scoreLabel.text = Score.ToString();
    }

    public void Play()
    {
        State = GameplayState.Playing;
        _homeContainer.SetActive(false);
        _gameOverUIContainer.SetActive(false);
        _gameOverNewHighscoreLabel.gameObject.SetActive(false);
        StopCoroutine("NewHighscoreAnimation");

        ShipManager.Instance.transform.position = ShipInitialPosition;
        ShipManager.Instance.gameObject.SetActive(true);
        ShipManager.Instance.StartShooting();
        Score = 0;
        _scoreLabel.text = Score.ToString();
        _hudContainer.SetActive(true);

        StartCoroutine(ChangeMusic(_gameplayMusic));

        LevelManager.Instance.StartLevel();
    }

    public void Pause()
    {
        State = GameplayState.Paused;
        _pauseButton.SetActive(false);
        _resumeButton.SetActive(true);
    }

    public void Resume()
    {
        State = GameplayState.Playing;
        _pauseButton.SetActive(true);
        _resumeButton.SetActive(false);
    }

    public void GameOver()
    {
        State = GameplayState.Idle;

        ShipManager.Instance.gameObject.SetActive(false);
        _hudContainer.SetActive(false);
        LevelManager.Instance.StopLevel();
        ScoreEffectsManager.Instance.Clear();

        int highscore = PlayerPrefs.GetInt("Highscore");
        if (highscore < Score)
        {
            PlayerPrefs.SetInt("Highscore", Score);
            highscore = Score;
            _gameOverNewHighscoreLabel.gameObject.SetActive(true);
            StartCoroutine("NewHighscoreAnimation");
        }

        _gameOverScoreLabel.text = GameOverScorePrefix + Score.ToString();
        _gameOverHighscoreLabel.text = highscore.ToString();
        _gameOverUIContainer.SetActive(true);

        StartCoroutine(ChangeMusic(_menuMusic));
    }

    private IEnumerator ChangeMusic(AudioClip clip)
    {
        float fadeTime = 1f;
        AudioSource source = GetComponent<AudioSource>();

        float time = 0;
        while (time < fadeTime * 0.5f)
        {
            time += Time.deltaTime;
            source.volume = Mathf.Lerp(1, 0, time / (fadeTime * 0.5f));
            yield return null;
        }

        source.Stop();
        source.clip = clip;
        source.Play();

        time = 0;
        while (time < fadeTime * 0.5f)
        {
            time += Time.deltaTime;
            source.volume = Mathf.Lerp(0, 1, time / (fadeTime * 0.5f));
            yield return null;
        }
    }

    private IEnumerator NewHighscoreAnimation()
    {
        float sizeChange = 0.2f;
        float sizeChangeTime = 0.4f;
        float time;

        while(true)
        {
            time = 0;
            while(time < sizeChangeTime)
            {
                _gameOverNewHighscoreLabel.localScale = Vector3.Lerp(Vector3.one, Vector3.one * (1 - sizeChange), time / sizeChangeTime);
                time += Time.deltaTime;
                yield return null;
            }
            time = 0;
            while (time < sizeChangeTime)
            {
                _gameOverNewHighscoreLabel.localScale = Vector3.Lerp(Vector3.one * (1 - sizeChange), Vector3.one, time / sizeChangeTime);
                time += Time.deltaTime;
                yield return null;
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && State == GameplayState.Idle)
        {
            Application.Quit();
        }
    }
}
