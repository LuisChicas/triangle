﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    private const float Lifetime = 0.4f;
    private const float MinDistance = 0.6f;
    private const float MaxDistance = 0.9f;
    private const float InitialScale = 0.9f;
    private const float FinalScale = 0.6f;

    private Transform[] _particles;
    private Vector3 _initialPosition;

    private void Start()
    {
        Play();
    }

    public void Play()
    {
        _particles = GetComponentsInChildren<Transform>().Skip(1).ToArray();
        _initialPosition = transform.position;
        StartCoroutine(Explode());
    }

    private IEnumerator Explode()
    {
        var directions = new Vector3[_particles.Length];
        int particlesPerQuadrant = (int)(_particles.Length / 4);
        float angleInRadians;
        float distance;
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < particlesPerQuadrant; j++)
            {
                angleInRadians = Random.Range(0.5f * i, 0.5f * (i + 1)) * Mathf.PI;
                distance = Random.Range(MinDistance, MaxDistance);
                directions[(i * particlesPerQuadrant) + j] = new Vector3(Mathf.Cos(angleInRadians) * distance, Mathf.Sin(angleInRadians) * distance);
            }
        }

        var sprites = new SpriteRenderer[_particles.Length];
        for (int i = 0; i < _particles.Length; i++)
        {
            sprites[i] = _particles[i].GetComponent<SpriteRenderer>();
        }
        var initialColor = sprites[0].color;
        var transparentColor = new Color(initialColor.r, initialColor.g, initialColor.b, 0);

        float time = 0;
        while(time < Lifetime)
        {
            time += Time.deltaTime;
            for(int i = 0; i < _particles.Length; i++)
            {
                _particles[i].position = _initialPosition + Vector3.Lerp(Vector3.zero, directions[i], time / Lifetime);
                _particles[i].localScale = Vector3.Lerp(Vector3.one * InitialScale, Vector3.one * FinalScale, time / Lifetime);
                sprites[i].color = Color.Lerp(initialColor, transparentColor, time / Lifetime);
            }
            yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
        }

        Destroy(gameObject);
	}
}
