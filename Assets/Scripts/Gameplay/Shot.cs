﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
    [HideInInspector]
    public float Speed;

	private void Start ()
    {
        Speed = 8;
        StartCoroutine(Move());
	}
	
    private IEnumerator Move()
    {
        while (true)
        {
            transform.Translate(Vector3.up * Speed * Time.deltaTime);
            yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("item"))
        {
            collision.GetComponent<Item>().OnHit();

            if (collision.GetComponent<Item>().Name != ItemName.FrecuenceBooster)
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
