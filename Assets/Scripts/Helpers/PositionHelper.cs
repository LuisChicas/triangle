﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionHelper
{
    public static Vector2 WorldToCanvasPosition(Vector3 worldPosition, Camera camera, Canvas canvas)
    {
        var topRightScreenPosition = new Vector3(Screen.width, Screen.height, camera.nearClipPlane);
        Vector3 topRightWorldPosition = camera.ScreenToWorldPoint(topRightScreenPosition);

        var relativePosition = new Vector2(
            worldPosition.x / topRightWorldPosition.x, 
            worldPosition.y / topRightWorldPosition.y);

        Vector2 topRightCanvasPosition = canvas.GetComponent<RectTransform>().sizeDelta / 2;

        return topRightCanvasPosition * relativePosition;
    }

    public static Vector2 WorldToCanvasPosition(Vector3 worldPosition, Canvas canvas)
    {
        return WorldToCanvasPosition(worldPosition, Camera.main, canvas);
    }
}
