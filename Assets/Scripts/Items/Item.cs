﻿using UnityEngine;
using System.Collections.Generic;

public class Item : MonoBehaviour
{
    [SerializeField]
    private ItemName _name;
    public ItemName Name
    {
        get { return _name; }
        private set { _name = value; }
    }

    public bool IsOnScreen;

    public virtual void Init(Dictionary<string, object> parameters = null)
    {
        IsOnScreen = true;
    }

    public virtual void OnHit() { }
}
