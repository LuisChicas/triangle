﻿
public enum ItemName
{
    Asteroid,
    BouncingAsteroid,
    DoubleAsteroid,
    TripleAsteroid,
    Spawner,
    Bomb,
    FrecuenceBooster,
    LongAsteroid,
    Comet,
    WideAsteroid
}