﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : Item
{
    public static readonly Color LighterColor = new Color(1, 1, 1);
    public static readonly Color DarkerColor = new Color(0.75f, 0.75f, 0.75f);

    public const int Score = 10;
    public const int MaxRotation = 50;

    [SerializeField]
    private GameObject _explosionPrefab;

    public override void Init(Dictionary<string, object> parameters = null)
    {
        base.Init();
        GetComponent<SpriteRenderer>().color = Color.Lerp(LighterColor, DarkerColor, Random.Range(0f, 1f));
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-MaxRotation, MaxRotation)));
        transform.localScale = Vector3.one * Random.Range(0.925f, 1.1f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ship"))
        {
            GameplayManager.Instance.GameOver();
        }
    }

    public override void OnHit()
    {
        base.OnHit();

        if (IsOnScreen)
        {
            GameplayManager.Instance.AddScore(Score);
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);

            GameObject explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            explosion.GetComponent<Explosion>().Play();

            ScoreEffectsManager.Instance.ShowAdditionEffect(transform.position, Score);
        }
    }

    private void OnBecameInvisible()
    {
        if (IsOnScreen)
        {
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);
        }
    }
}
