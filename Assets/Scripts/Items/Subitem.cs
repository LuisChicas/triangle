﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Subitem : Item
{
    public Item Parent { get; set; }

    public override void OnHit()
    {
        Parent.OnHit();
    }
}
