﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comet : Item
{
    private const float MaxHorizontalSpeed = 5.5f;
    private const float MaxExtraVerticalSpeed = 1.3f;

    [SerializeField]
    private SpriteRenderer _tailRenderer;
    [SerializeField]
    private Sprite[] _tailSprites;

    private Vector3 _movement = new Vector3();

    public override void Init(Dictionary<string, object> parameters = null)
    {
        base.Init();

        if (parameters != null && parameters.ContainsKey("minAngle"))
        {
            float minAngle = float.Parse(parameters["minAngle"].ToString());
            minAngle = Mathf.Clamp(minAngle, -90, 90);

            float maxAngle = float.Parse(parameters["maxAngle"].ToString());
            maxAngle = Mathf.Clamp(maxAngle, minAngle, 90);

            float angle = Random.Range(minAngle, maxAngle);

            transform.rotation = Quaternion.Euler(Vector3.forward * angle);

            // No speed for an angle of 0.
            // Full speed for an angle of 90 or -90.
            _movement.x = MaxHorizontalSpeed * (angle / 90);

            // Full extra speed for an angle of 0.
            // No extra speed for an angle of 90 or -90.
            _movement.y = -MaxExtraVerticalSpeed * ((90 - Mathf.Abs(angle)) / 90);
        }
        else
        {
            _movement = new Vector3(0, -MaxExtraVerticalSpeed);
        }

        StartCoroutine("AnimateTail");
    }

    private void Update()
    {
        if (gameObject.activeSelf && GameplayManager.Instance.State == GameplayState.Playing)
        {
            transform.Translate(_movement * Time.deltaTime, Space.World);
        }
    }

    private IEnumerator AnimateTail()
    {
        int i = 0;
        while(true)
        {
            _tailRenderer.sprite = _tailSprites[i];
            yield return new WaitForSeconds(0.25f);
            i = i + 1 < _tailSprites.Length ? i + 1 : 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ship"))
        {
            GameplayManager.Instance.GameOver();
        }
    }

    private void OnBecameInvisible()
    {
        if (IsOnScreen)
        {
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);
            StopCoroutine("AnimateTail");
        }
    }
}
