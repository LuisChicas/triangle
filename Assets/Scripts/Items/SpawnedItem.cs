﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnedItem : MonoBehaviour
{
    public void Move(float angle)
    {
        StartCoroutine(MoveRoutine(angle));
    }

    private IEnumerator MoveRoutine(float angle)
    {
        float time = 0.4f;
        float currentTime = 0;
        float distance = Random.Range(0.6f, 0.9f);
        float currentDistance = 0;
        float previousDistance = 0;
        Vector3 direction = new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));

        while (currentTime < time && gameObject.activeSelf)
        {
            currentTime += Time.deltaTime;
            previousDistance = currentDistance;
            currentDistance = Mathf.SmoothStep(-distance, distance, 0.5f + (0.5f * (currentTime / time)));
            transform.Translate(direction * (currentDistance - previousDistance), Space.World);
            yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
        }

        Destroy(this);
    }    
}
