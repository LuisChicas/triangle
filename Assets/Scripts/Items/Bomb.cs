﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : Item
{
    public const int Score = 20;
    private const int MaxRotation = 10;

    [SerializeField]
    private GameObject _explosionPrefab;
    [SerializeField]
    private GameObject _waveCollider;
    [SerializeField]
    private SpriteRenderer _waveSprite;
    [SerializeField]
    private SpriteRenderer _lights;
    
    public override void Init(Dictionary<string, object> parameters = null)
    {
        base.Init();
        _lights.gameObject.SetActive(true);
        StartCoroutine(Glow());
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-MaxRotation, MaxRotation)));
    }

    private IEnumerator Glow()
    {
        float time = 0.5f;
        float currentTime = 0;
        Color solidColor = Color.white;
        Color transparentColor = new Color(1, 1, 1, 0.3f);
        while(IsOnScreen)
        {
            currentTime = 0;
            while (currentTime < time)
            {
                currentTime += Time.deltaTime;
                _lights.color = Color.Lerp(solidColor, transparentColor, currentTime / time);
                yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
            }

            currentTime = 0;
            while (currentTime < time)
            {
                currentTime += Time.deltaTime;
                _lights.color = Color.Lerp(transparentColor, solidColor, currentTime / time);
                yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ship"))
        {
            GameplayManager.Instance.GameOver();
        }
    }

    private IEnumerator Explode()
    {
        _waveCollider.SetActive(true);

        float time = 0.125f;
        float currentTime = 0;

        Color solidColor = new Color(1, 1, 1, 1);
        Color transparentColor = new Color(1, 1, 1, 0);
        _waveSprite.color = transparentColor;
        _waveSprite.gameObject.SetActive(true);

        while (currentTime < time)
        {
            currentTime += Time.deltaTime;
            _waveSprite.color = Color.Lerp(transparentColor, solidColor, currentTime / time);
            yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
        }

        currentTime = 0;

        while (currentTime < time)
        {
            currentTime += Time.deltaTime;
            _waveSprite.color = Color.Lerp(solidColor, transparentColor, currentTime / time);
            yield return new WaitUntil(() => GameplayManager.Instance.State == GameplayState.Playing);
        }

        _waveSprite.gameObject.SetActive(false);
        _waveCollider.SetActive(false);

        LevelManager.Instance.RemoveItem(transform);
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<Collider2D>().enabled = true;
    }

    public override void OnHit()
    {
        base.OnHit();
        if (IsOnScreen)
        {
            GameplayManager.Instance.AddScore(Score);
            IsOnScreen = false;
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;
            _lights.gameObject.SetActive(false);
            StartCoroutine(Explode());

            GameObject explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            explosion.GetComponent<Explosion>().Play();

            ScoreEffectsManager.Instance.ShowAdditionEffect(transform.position, Score);
        }
    }

    private void OnBecameInvisible()
    {
        if (IsOnScreen)
        {
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);
        }
    }
}
