﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncingAsteroid : Item
{
    public const int Score = 10;
    private const float HorizontalSpeed = 2.5f;
    private const float ExtraVerticalSpeed = 0.5f;
    private const int MaxRotation = 35;
    private readonly Color LighterColor = new Color(1, 1, 1);
    private readonly Color DarkerColor = new Color(0.8f, 0.8f, 0.8f);

    [SerializeField]
    private GameObject _explosionPrefab;

    private float _screenBoundary;
    private Vector3 _movement = new Vector3();

    private void Start()
    {
        _screenBoundary = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Camera.main.nearClipPlane)).x;
        _screenBoundary = Mathf.Abs(_screenBoundary);

        _movement.y = -ExtraVerticalSpeed;
    }

    public override void Init(Dictionary<string, object> parameters = null)
    {
        base.Init();

        _movement.x = Random.Range(0f, 1f) > 0.5f ? HorizontalSpeed : -HorizontalSpeed;

        if (parameters != null && parameters.ContainsKey("direction"))
        {
            if (parameters["direction"].ToString() == "left")
            {
                _movement.x = -HorizontalSpeed;
            }
            else if (parameters["direction"].ToString() == "right")
            {
                _movement.x = HorizontalSpeed;
            }
        }

        GetComponent<SpriteRenderer>().color = Color.Lerp(LighterColor, DarkerColor, Random.Range(0f, 1f));
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-MaxRotation, MaxRotation)));
    }

    private void Update()
    {
        if (gameObject.activeSelf && GameplayManager.Instance.State == GameplayState.Playing)
        {
            transform.Translate(_movement * Time.deltaTime, Space.World);

            if (Mathf.Abs(transform.position.x) > _screenBoundary 
                && Mathf.Sign(transform.position.x) == Mathf.Sign(_movement.x))
            {
                _movement.x *= -1;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ship"))
        {
            GameplayManager.Instance.GameOver();
        }
    }

    public override void OnHit()
    {
        base.OnHit();
        if (IsOnScreen)
        {
            GameplayManager.Instance.AddScore(Score);
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);

            GameObject explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            explosion.GetComponent<Explosion>().Play();

            ScoreEffectsManager.Instance.ShowAdditionEffect(transform.position, Score);
        }
    }

    private void OnBecameInvisible()
    {
        if (IsOnScreen)
        {
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);
        }
    }
}
