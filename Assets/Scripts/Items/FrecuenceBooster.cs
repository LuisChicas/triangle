﻿using UnityEngine;

public class FrecuenceBooster : Item
{
    private const float BoosterValue = 0.3f;

    [SerializeField]
    private AudioClip _pickClip;
    [SerializeField]
    private GameObject _sfxItemPrefab;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ship"))
        {
            ShipManager.Instance.AddShotsPerSecond(BoosterValue);
            LevelManager.Instance.RemoveItemPermanently(transform);

            GameObject sfx = Instantiate(_sfxItemPrefab) as GameObject;
            sfx.GetComponent<SFXItem>().Play(_pickClip);

            Destroy(gameObject);
        }
    }
}
