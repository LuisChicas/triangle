﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleAsteroid : Item
{
    public static readonly Color Color = new Color(0.65f, 0.65f, 0.65f);

    public const int Score = 20;
    public const int MaxRotation = 20;

    [SerializeField]
    private Subitem _mediumAsteroid;
    [SerializeField]
    private Subitem _smallAsteroid;
    [SerializeField]
    private GameObject _explosionPrefab;

    private bool IsShootedOnce;

    public void Start()
    {
        _mediumAsteroid.Parent = this;
        _smallAsteroid.Parent = this;
    }

    public override void Init(Dictionary<string, object> parameters = null)
    {
        base.Init();
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-MaxRotation, MaxRotation)));

        _smallAsteroid.GetComponent<SpriteRenderer>().color = Color.Lerp(Asteroid.LighterColor, Asteroid.DarkerColor, Random.Range(0f, 1f));

        _mediumAsteroid.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-MaxRotation, MaxRotation)));
        _smallAsteroid.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-Asteroid.MaxRotation, Asteroid.MaxRotation)));

        _mediumAsteroid.gameObject.SetActive(true);
        _smallAsteroid.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ship"))
        {
            GameplayManager.Instance.GameOver();
        }
    }

    public override void OnHit()
    {
        base.OnHit();
        if (IsOnScreen)
        {
            if (IsShootedOnce)
            {
                GameplayManager.Instance.AddScore(Score);
                IsOnScreen = false;
                LevelManager.Instance.RemoveItem(transform);

                ScoreEffectsManager.Instance.ShowAdditionEffect(transform.position, Score);

                IsShootedOnce = false;
            }
            else
            {
                _mediumAsteroid.gameObject.SetActive(false);
                _smallAsteroid.gameObject.SetActive(true);

                IsShootedOnce = true;
            }

            GameObject explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            explosion.GetComponent<Explosion>().Play();
        }
    }

    private void OnBecameInvisible()
    {
        if (IsOnScreen)
        {
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);
        }
    }
}
