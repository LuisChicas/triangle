﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner : Item
{
    public const int Score = 10;
    private const int MaxRotation = 20;

    [SerializeField]
    private GameObject _explosionPrefab;

    public override void Init(Dictionary<string, object> parameters = null)
    {
        base.Init();
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-MaxRotation, MaxRotation)));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ship"))
        {
            GameplayManager.Instance.GameOver();
        }
    }

    public override void OnHit()
    {
        base.OnHit();
        if (IsOnScreen)
        {
            Transform spawnedItem = LevelManager.Instance.GenerateItem(ItemName.Asteroid);
            spawnedItem.position = transform.position + Vector3.right * -0.1f;
            spawnedItem.gameObject.AddComponent<SpawnedItem>().Move(Random.Range(210, 250));

            spawnedItem = LevelManager.Instance.GenerateItem(ItemName.Asteroid);
            spawnedItem.position = transform.position + Vector3.right * 0.1f;
            spawnedItem.gameObject.AddComponent<SpawnedItem>().Move(Random.Range(290, 330));

            GameplayManager.Instance.AddScore(Score);
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);

            GameObject explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            explosion.GetComponent<Explosion>().Play();

            ScoreEffectsManager.Instance.ShowAdditionEffect(transform.position, Score);
        }
    }

    private void OnBecameInvisible()
    {
        if (IsOnScreen)
        {
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);
        }
    }
}
