﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleAsteroid : Item
{
    public const int Score = 40;
    private const int MaxRotation = 20;
    private readonly Color Color = new Color(0.5f, 0.5f, 0.5f);

    [SerializeField]
    private Subitem _bigAsteroid;
    [SerializeField]
    private Subitem _mediumAsteroid;
    [SerializeField]
    private Subitem _smallAsteroid;
    [SerializeField]
    private GameObject _explosionPrefab;

    private int _shots;

    private void Start()
    {
        _bigAsteroid.Parent = this;
        _mediumAsteroid.Parent = this;
        _smallAsteroid.Parent = this;
    }

    public override void Init(Dictionary<string, object> parameters = null)
    {
        base.Init();
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-MaxRotation, MaxRotation)));

        _smallAsteroid.GetComponent<SpriteRenderer>().color = Color.Lerp(Asteroid.LighterColor, Asteroid.DarkerColor, Random.Range(0f, 1f));

        _bigAsteroid.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-MaxRotation, MaxRotation)));
        _mediumAsteroid.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-DoubleAsteroid.MaxRotation, DoubleAsteroid.MaxRotation)));
        _smallAsteroid.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-Asteroid.MaxRotation, Asteroid.MaxRotation)));

        _bigAsteroid.gameObject.SetActive(true);
        _mediumAsteroid.gameObject.SetActive(false);
        _smallAsteroid.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ship"))
        {
            GameplayManager.Instance.GameOver();
        }
    }

    public override void OnHit()
    {
        base.OnHit();

        if (IsOnScreen)
        {
            _shots++;

            if (_shots == 3)
            {
                GameplayManager.Instance.AddScore(Score);
                IsOnScreen = false;
                LevelManager.Instance.RemoveItem(transform);

                ScoreEffectsManager.Instance.ShowAdditionEffect(transform.position, Score);

                _shots = 0;
            }
            else if (_shots == 2)
            {
                _mediumAsteroid.gameObject.SetActive(false);
                _smallAsteroid.gameObject.SetActive(true);
            }
            else
            {
                _bigAsteroid.gameObject.SetActive(false);
                _mediumAsteroid.gameObject.SetActive(true);
            }

            GameObject explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            explosion.GetComponent<Explosion>().Play();
        }
    }

    private void OnBecameInvisible()
    {
        if (IsOnScreen)
        {
            IsOnScreen = false;
            LevelManager.Instance.RemoveItem(transform);
        }
    }
}
