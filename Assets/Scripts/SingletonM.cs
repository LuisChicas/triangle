﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Singleton for MonoBehaviour classes
/// </summary>

// TODO: Refactor for prevent dynamic instantiation when is not desired
public class SingletonM<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T instance;

	public static T Instance
	{
		get
		{
			if (instance == null) 
			{
				CreateInstance();
			}
			return instance;
		}
	}

	[SerializeField]
	private bool InScene = false;

	private static void CreateInstance () 
	{
		var singleton = FindObjectOfType<T>();

		if (singleton != null) 
		{
			instance = singleton.GetComponent<T>();
		}
	}

	protected virtual void Awake()
	{
		if (InScene && instance == null)
		{
			instance = gameObject.GetComponent<T>();
		}
	}
}