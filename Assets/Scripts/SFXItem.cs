﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXItem : MonoBehaviour
{
    public void Play(AudioClip clip)
    {
        GetComponent<AudioSource>().clip = clip;
        GetComponent<AudioSource>().Play();
        Destroy(gameObject, clip.length);
    }
}
