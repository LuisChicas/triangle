﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreAdditionEffect : MonoBehaviour
{
    [SerializeField]
    private Text _scoreForeground;

    public void Show(int score)
    {
        _scoreForeground.text = score.ToString();

        StartCoroutine("RunAnimation");
    }

    private IEnumerator RunAnimation()
    {
        GetComponent<Animation>().Play();

        yield return new WaitForSeconds(GetComponent<Animation>().clip.length);

        gameObject.SetActive(false);
    }
}
