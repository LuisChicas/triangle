﻿using UnityEditor;
using UnityEngine;

public class LeveliumTools : EditorWindow
{
	[MenuItem("Levelium/Tools")]
	public static void Init ()
	{
		EditorWindow.GetWindow (typeof(LeveliumTools));
	}

	private void OnGUI ()
	{
		AddClearPlayerPrefs ();
	}

	private void AddClearPlayerPrefs()
	{
		GUILayout.Label ("PlayerPrefs", EditorStyles.boldLabel);

		if (GUILayout.Button ("Clear PlayerPrefs"))
		{
			PlayerPrefs.DeleteAll();
		}
	}
}