﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebugScreen : MonoBehaviour
{
	[SerializeField]
	private RectTransform _logPanel;
	[SerializeField]
	private Text _log;
	[SerializeField]
	private Text _tabText;
	[SerializeField]
	private bool _openOnInit = true;

	private bool _isOpen;

	private void Awake ()
	{
		Application.logMessageReceived += Log;
	}

	private void Start () 
	{		
		if (_openOnInit) 
		{
			Open ();
		}
	}

	private void Log (string condition, string stackTrace, LogType type)
	{
		if (_log.text.Split ('\n').Length >= 8) 
		{
			string[] lines = _log.text.Split ('\n');
			for (int i = 0; i < 4; i++) 
			{
				_log.text += lines [i] + "\n";
			} 
		}

		_log.text += condition + "\n";
	}

	public void OnTab ()
	{
		if (_isOpen) 
		{
			Close ();
		}
		else 
		{
			Open ();
		}
	}

	private void Close ()
	{
		_isOpen = false;
		_logPanel.anchorMax = new Vector2 (0, _logPanel.anchorMax.y);
		_logPanel.anchoredPosition = new Vector2 (0, _logPanel.anchoredPosition.y);
		_logPanel.sizeDelta = new Vector2 (100, _logPanel.sizeDelta.y);
		_tabText.text = ">";
	}

	private void Open ()
	{
		_isOpen = true;
		_logPanel.anchorMax = new Vector2 (1, _logPanel.anchorMax.y);
		_logPanel.anchoredPosition = new Vector2 (-100, _logPanel.anchoredPosition.y);
		_logPanel.sizeDelta = new Vector2 (-100, _logPanel.sizeDelta.y);
		_tabText.text = "<";
	}

	private void OnDestroy ()
	{		
		Application.logMessageReceived -= Log;
	}
}